#!/usr/bin/env python3.6
#
# Copyright (C) 2019    Jochen Welzel (jwelzel@j10l.de)
#
# Check plugin for check_mk to create output for bhyve_vms
# check_mk check
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 
# 02110-1301, USA.
#

import os
import re

print('<<<bhyve_vms>>>')
vms = os.popen("vm list | tail -n +2").read().splitlines()
for line in vms:
    print(re.sub(r'([^ ]) ([^ ])',r'\1_\2',line))

