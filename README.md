## Check_mk Plugin for Bhyve VMs

At the moment this plugin checks the output of vm-bhyve's
vm list command.  
It throws a critical, if an autostart-vm is not running. 
Warnings if no vm is running or one or more vms are in bootloader
state.
