#!/usr/bin/python
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
# Copyright (C) 2019    Jochen Welzel (jwelzel@j10l.de)
#
# Check_mk script to check the status of FreeBSD Bhyve VMs
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
#
# Sample Plugin output:
# <<<bhyve_vms>>>
# android01       default    grub       2    2048M   -    No         Stopped
# contao01        default    grub       2    4096M   -    No         Running_(46772)
# fbsd-desk01     default    bhyveload  2    4096M   -    No         Stopped
# fw02.zelnet.de  default    bhyveload  2    2048M   -    Yes_[1]    Running_(44062)
# git01           default    grub       1    512M    -    Yes_[3]    Running_(98028)
# nagios01        default    grub       2    4096M   -    No         Running_(46176)
# nix01           default    none       2    4096M   -    No         Stopped
# ns03            default    grub       1    512M    -    Yes_[2]    Running_(6593)
# ns03-old        default    grub       1    512M    -    No         Stopped
# obsd-test       default    grub       1    256M    -    No         Stopped
# test01          default    grub       2    4096M   -    No         Stopped
# wiki01          default    grub       1    512M    -    Yes_[4]    Running_(3576)

def check_bhyve_vms(item, params, info):
    lc = 0
    vmc = 0
    asnr = 0
    blvm = 0
    for line in info:
        lc += 1
        if "Running" in line[7]:
            vmc += 1
        elif "Bootloader" in line[7]:
            blvm += 1
        if "Yes" in line[6]:
            if "Running" not in line[7]:
                asnr += 1
    perf = [ ("running_vms", vmc), ("total_vms", lc)]
    if asnr > 0:
        return (2, "%d VMs in autostart but not running" % asnr, perf)
    elif blvm > 0:
        return (1, "%d VMs in state bootloader" % blvm, perf)
    elif vmc == 0:
        return (1, "0 VMs running", perf)
    result = str(vmc)+" of "+str(lc)+" VMs running"
    return (0, "%d of %d VMs running" % (vmc, lc), perf)

def inventory_bhyve_vms(info):
    #infolines=info.splitlines()
    #for line in infolines:
    #    yield line
    return [(None, None)]


check_info["bhyve_vms"] = {
    'check_function'        : check_bhyve_vms,
    'inventory_function'    : inventory_bhyve_vms,
    'service_description'   : 'Bhyve VMs',
    'has_perfdata'          : True,
    'group'                 : 'bhyve_vms',
    'includes'              : [ ],
}
